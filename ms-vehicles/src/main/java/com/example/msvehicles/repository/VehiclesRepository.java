package com.example.msvehicles.repository;

import com.example.msvehicles.entity.Vehicles;
import org.springframework.data.repository.CrudRepository;

public interface VehiclesRepository extends CrudRepository<Vehicles, Integer> {
}

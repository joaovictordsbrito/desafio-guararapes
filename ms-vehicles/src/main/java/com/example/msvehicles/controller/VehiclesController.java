package com.example.msvehicles.controller;

import com.example.msvehicles.entity.Vehicles;
import com.example.msvehicles.repository.VehiclesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/vehicles")
public class VehiclesController {

    @Autowired
    private VehiclesRepository vehiclesRepository;

    @RequestMapping(method = RequestMethod.POST)
    Vehicles create (@RequestBody Vehicles vehicle){
        return vehiclesRepository.save(vehicle);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveAll")
    Iterable<Vehicles> saveAll(@RequestBody Iterable<Vehicles> vehicles){
        return vehiclesRepository.saveAll(vehicles);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    Optional<Vehicles> findbyId(@PathVariable Integer id){
        return vehiclesRepository.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    Iterable<Vehicles> findAll(){
        return vehiclesRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT)
    Vehicles update (@RequestBody Vehicles vehicles){
        return vehiclesRepository.save(vehicles);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    void deleteById(@PathVariable Integer id){
        vehiclesRepository.deleteById(id);
    }
}

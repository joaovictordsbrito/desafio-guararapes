package com.example.gateway.Config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

@Primary
@Configuration
public class SwaggerConfig implements SwaggerResourcesProvider {
    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();
        resources.add(swaggerResource("films", "/films/v2/api-docs", "2.0"));
        resources.add(swaggerResource("planets", "/planets/v2/api-docs", "2.0"));
        resources.add(swaggerResource("species", "/species/v2/api-docs", "2.0"));
        resources.add(swaggerResource("starships", "/starships/v2/api-docs", "2.0"));
        resources.add(swaggerResource("vehicles", "/vehicles/v2/api-docs", "2.0"));
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}



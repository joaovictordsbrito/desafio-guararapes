# Desafio Guararapes

A Aplicação foi desenvolvida utilizando Java 11, com base Java EE. Utilizando a arquitetura de microsserviços no back-end e angular no front-end. Aplicação feita levando em conta as diretrizes do desafio de desenvolvimento da etapa de recrutamento da empresa Guararapes. Desafio: https://github.com/Guararapes/desafio

O banco utilizado foi o PostgreSql, utilizando a conexão local. E as tabelas utilizadas serão criadas no momento em que as aplicações forem executadas individualmente. 

Existem 8 microsserviços criados e configurados nesse projeto. Sendo 5 API's (ms-films, ms-planets, ms-species, ms-starships, ms-vehicles) os microsserviços que retém a camada mais próxima ao banco, efetuando a carga e descarga de dados diretamente no banco. E 3 microsserviços de composição/integração (integrador, gateway e service Discovery). Documentação das API's feita pelo Swagger, utilizando o Spring Fox.

O Front-End foi feito utilizando angular como base, cada API tendo sua representação no front por meio das rotas oferecidas do Angular. O Front por sua vez se comunica diretamente com o gateway que retém as informações de rota dentro do back-end. O Service discovery age como ponte entre as API's e os microsserviços de integração. E o integrador serve como um serviço para dar uma "carga inicial" no banco de dados, consumindo a API externa do SWAPI e redirecionando cada tipo de dado a seu devido microsserviço.

Bibliotecas Utilizadas:
gson (pasta LIBS)

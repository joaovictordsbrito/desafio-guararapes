package com.example.msplanets.controller;

import com.example.msplanets.entity.Planets;
import com.example.msplanets.repository.PlanetsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/planets")
public class PlanetsController {

    @Autowired
    private PlanetsRepository planetsRepository;

    @RequestMapping(method = RequestMethod.POST)
    Planets create (@RequestBody Planets planet){
        return planetsRepository.save(planet);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveAll")
    Iterable<Planets> saveAll(@RequestBody Iterable<Planets> planets){
        return planetsRepository.saveAll(planets);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    Optional<Planets> findById(@PathVariable Integer id){
        return planetsRepository.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    Iterable<Planets> findAll(){
        return planetsRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT)
    Planets update (@RequestBody Planets planets){
        return planetsRepository.save(planets);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    void deleteById(@PathVariable Integer id){
        planetsRepository.deleteById(id);
    }
}

package com.example.msplanets.repository;

import com.example.msplanets.entity.Planets;
import org.springframework.data.repository.CrudRepository;

public interface PlanetsRepository extends CrudRepository<Planets, Integer> {
}

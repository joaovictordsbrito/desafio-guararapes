package com.example.msspecies.repository;

import com.example.msspecies.entity.Species;
import org.springframework.data.repository.CrudRepository;

public interface SpeciesRepository extends CrudRepository<Species, Integer> {
}

package com.example.msspecies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan
@EnableJpaRepositories
@EnableEurekaClient
public class MsSpeciesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsSpeciesApplication.class, args);
	}

}

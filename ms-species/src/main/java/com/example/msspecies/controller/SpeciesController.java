package com.example.msspecies.controller;

import com.example.msspecies.entity.Species;
import com.example.msspecies.repository.SpeciesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/species")
public class SpeciesController {

    @Autowired
    private SpeciesRepository speciesRepository;

    @RequestMapping(method = RequestMethod.POST)
    Species create (@RequestBody Species specie){
        return speciesRepository.save(specie);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveAll")
    Iterable<Species> saveAll(@RequestBody Iterable<Species> species){
        return speciesRepository.saveAll(species);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    Optional<Species> findById(@PathVariable Integer id){
        return speciesRepository.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    Iterable<Species> findAll(){
        return speciesRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT)
    Species update (@RequestBody Species species){
        return speciesRepository.save(species);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    void deleteById(@PathVariable Integer id){
        speciesRepository.deleteById(id);
    }
}

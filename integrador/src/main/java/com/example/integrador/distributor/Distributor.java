package com.example.integrador.distributor;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.http.client.HttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Service
public class Distributor {
    Gson gson = new Gson();
    HttpClient httpClient = HttpClientBuilder.create().build();

    private static final String SAVE_ALL_FILMS = "http://localhost:8080/films/saveAll";
    private static final String SAVE_ALL_PLANETS = "http://localhost:8081/planets/saveAll";
    private static final String SAVE_ALL_SPECIES = "http://localhost:8082/species/saveAll";
    private static final String SAVE_ALL_STARSHIPS = "http://localhost:8083/starships/saveAll";
    private static final String SAVE_ALL_VEHICLES = "http://localhost:8084/vehicles/saveAll";

    public void persistAll(JsonArray results, String command) throws IOException {
        switch (command){
            case "films":
                StringEntity filmPosting = new StringEntity(gson.toJson(results.getAsJsonArray()));
                HttpPost filmPost = new HttpPost(SAVE_ALL_FILMS);
                filmPost.setEntity(filmPosting);
                filmPost.setHeader("Content-type", MediaType.APPLICATION_JSON);
                HttpResponse response = httpClient.execute(filmPost);
                break;
            case "planets":
                StringEntity planetsPosting = new StringEntity(gson.toJson(results.getAsJsonArray()));
                HttpPost planetsPost = new HttpPost(SAVE_ALL_PLANETS);
                planetsPost.setEntity(planetsPosting);
                planetsPost.setHeader("Content-type", MediaType.APPLICATION_JSON);
                HttpResponse planetsResponse = httpClient.execute(planetsPost);
                break;
            case "species":
                StringEntity speciesPostingString = new StringEntity(gson.toJson(results.getAsJsonArray()));
                HttpPost speciesPost = new HttpPost(SAVE_ALL_SPECIES);
                speciesPost.setEntity(speciesPostingString);
                speciesPost.setHeader("Content-type", MediaType.APPLICATION_JSON);
                HttpResponse speciesResponse = httpClient.execute(speciesPost);
                break;

            case "starships":
                StringEntity starshipPostingString = new StringEntity(gson.toJson(results.getAsJsonArray()));
                HttpPost starshipPost = new HttpPost(SAVE_ALL_STARSHIPS);
                starshipPost.setEntity(starshipPostingString);
                starshipPost.setHeader("Content-type", MediaType.APPLICATION_JSON);
                HttpResponse starshipResponse = httpClient.execute(starshipPost);
                break;

            case "vehicles":
                StringEntity vehiclesPostingString = new StringEntity(gson.toJson(results.getAsJsonArray()));
                HttpPost vehiclesPost = new HttpPost(SAVE_ALL_VEHICLES);
                vehiclesPost.setEntity(vehiclesPostingString);
                vehiclesPost.setHeader("Content-type", MediaType.APPLICATION_JSON);
                HttpResponse vehiclesResponse = httpClient.execute(vehiclesPost);
                break;
        }
    }
}

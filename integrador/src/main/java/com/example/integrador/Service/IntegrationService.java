package com.example.integrador.Service;

import com.example.integrador.distributor.Distributor;
import com.example.integrador.entitys.SwapiCommands;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class IntegrationService {
    JsonObject jsonObject = new JsonObject();
    JsonArray results = new JsonArray();
    private static final RestService api = new RestService();

    @Autowired
    Distributor distributor;

    @Autowired
    SwapiService swapiService;

    public String integration(String command) throws IOException {
        if(SwapiCommands.contains(command)) {
            results = swapiService.getAll(command);
            distributor.persistAll(results, command);
            return("Pronto! " + command + " Atualizado com sucesso!");
        }else{
            return ("Comando Inválido");
        }
    }
}

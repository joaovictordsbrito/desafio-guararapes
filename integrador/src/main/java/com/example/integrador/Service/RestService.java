package com.example.integrador.Service;

import com.google.gson.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

@Service
public class RestService {

    public JsonArray getBuilder(String path) throws IOException {
        HttpGet httpGet;
        httpGet = new HttpGet("http://swapi.dev/api/" + path +  "/");
        return getRequest(httpGet);
    }

    public JsonArray getRequest(HttpGet getRequest) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        getRequest.addHeader("Cookie", MediaType.APPLICATION_JSON);
        HttpResponse response = httpClient.execute(getRequest);
        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        }

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

        String line;
        StringBuilder stringBuilder = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }

        JsonObject jsonObject = deserialize(stringBuilder.toString());
        bufferedReader.close();
        JsonArray results = setComponentIds(jsonObject);

        return results;
    }

    public JsonObject deserialize(String json) {
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonObject jsonClass = parser.parse(json).getAsJsonObject();
        return jsonClass;
    }

    public JsonArray setComponentIds(JsonObject jsonObject) {
        JsonArray results = jsonObject.getAsJsonArray("results");
        for(int i = 0; i < results.size(); i++){
            String [] string = results.get(i).getAsJsonObject().get("url").toString().split("/");
            Integer id = Integer.parseInt(string[string.length - 2]);
            results.get(i).getAsJsonObject().addProperty("id", id);
        }
        return results;
    }
}

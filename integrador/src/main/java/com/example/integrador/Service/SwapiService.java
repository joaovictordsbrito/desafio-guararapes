package com.example.integrador.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;

@Service
public class SwapiService {
    private RestService restService;
    public SwapiService(RestService restService){
        this.restService = restService;
    }

    public JsonArray getAll(String path){
        JsonArray jsonArray = null;
        try {
            jsonArray = restService.getBuilder(path);
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonArray;
    }
}

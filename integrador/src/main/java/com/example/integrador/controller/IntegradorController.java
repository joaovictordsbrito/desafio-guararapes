package com.example.integrador.controller;

import com.example.integrador.Service.IntegrationService;
import com.example.integrador.distributor.Distributor;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


@RestController
@RequestMapping(path = "/")
public class IntegradorController {

    @Autowired
    IntegrationService integrationService;

    @GetMapping(path = "/{command}")
    public String getBuilder(@PathVariable String command) throws IOException {
        return integrationService.integration(command);
    }
}

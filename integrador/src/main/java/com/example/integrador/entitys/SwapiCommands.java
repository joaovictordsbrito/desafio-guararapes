package com.example.integrador.entitys;

public enum SwapiCommands {
    FILMS("films"), PLANETS("planets"), SPECIES("species"), STARSHIPS("starships"), VEHICLES("vehicles");

    SwapiCommands(String commands) {
        this.name = commands;
    }

    private String name;

    static public Boolean contains(String name){
        for(SwapiCommands command : SwapiCommands.values()){
            if(command.name.equals(name)){
                return true;
            }
        }
        return false;
    }
}

package com.example.msfilms.repository;

import com.example.msfilms.entity.Films;
import org.springframework.data.repository.CrudRepository;

public interface FilmsRepository extends CrudRepository<Films, Integer> {
}

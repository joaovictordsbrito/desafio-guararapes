package com.example.msfilms.controller;

import com.example.msfilms.entity.Films;
import com.example.msfilms.repository.FilmsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/films")
public class FilmsController {

    @Autowired
    private FilmsRepository filmsRepository;

    @RequestMapping(method = RequestMethod.POST)
    Films create (@RequestBody Films film){
        return filmsRepository.save(film);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveAll")
    Iterable<Films> createAll(@RequestBody Iterable<Films> films){
        return filmsRepository.saveAll(films);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    Optional<Films> findById(@PathVariable Integer id){
        return filmsRepository.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    Iterable<Films> findAll(){
        return filmsRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT)
    Films update (@RequestBody Films film){
        return filmsRepository.save(film);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    void deleteById(@PathVariable Integer id){
        filmsRepository.deleteById(id);
    }
}

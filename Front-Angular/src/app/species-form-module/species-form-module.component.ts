import { Router } from '@angular/router';
import { ComponentRegistrationService } from './../component-registration.service';
import { Specie } from './../models/specie';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-species-form-module',
  templateUrl: './species-form-module.component.html',
  styleUrls: ['./species-form-module.component.css']
})
export class SpeciesFormModuleComponent implements OnInit {
  public species: Specie;

  constructor(private _service: ComponentRegistrationService, private _router:Router) { }

  ngOnInit(): void {
    this.species = this._service.getter();
  }

  processForm(){
    this.splitArrays(this.species);

    if(this.species.id == undefined){
      this._service.createComponent(this.species, "species");
    }else{
      this._service.updateComponent(this._service, "species");
    }
    this._router.navigate(['species']);
  }

  router(component: string){
    this._router.navigate([component]);
  }

  splitArrays(specie: Specie){
    if(this.species.people != undefined){
      this.species.people = this.species.people.toString().split(',');
    }
    if(this.species.films != undefined){
      this.species.films = this.species.films.toString().split(',');
    }
  }

}

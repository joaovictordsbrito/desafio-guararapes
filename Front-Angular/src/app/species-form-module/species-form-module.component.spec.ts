import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeciesFormModuleComponent } from './species-form-module.component';

describe('SpeciesFormModuleComponent', () => {
  let component: SpeciesFormModuleComponent;
  let fixture: ComponentFixture<SpeciesFormModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpeciesFormModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeciesFormModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

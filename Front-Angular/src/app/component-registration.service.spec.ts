import { TestBed } from '@angular/core/testing';

import { ComponentRegistrationService } from './component-registration.service';

describe('ComponentRegistrationService', () => {
  let service: ComponentRegistrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComponentRegistrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

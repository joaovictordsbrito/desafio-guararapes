import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentRegistrationService } from './component-registration.service';
import { SearchModuleComponent } from './search-module/search-module.component';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { PlanetsModuleComponent } from './planets-module/planets-module.component';
import { SpeciesModuleComponent } from './species-module/species-module.component';
import { StarshipsModuleComponent } from './starships-module/starships-module.component';
import { VehiclesModuleComponent } from './vehicles-module/vehicles-module.component';
import { FilmFormModuleComponent } from './film-form-module/film-form-module.component';
import { PlanetFormModuleComponent } from './planet-form-module/planet-form-module.component';
import { SpeciesFormModuleComponent } from './species-form-module/species-form-module.component';
import { StarshipsFormModuleComponent } from './starships-form-module/starships-form-module.component';
import { VehiclesFormModuleComponent } from './vehicles-form-module/vehicles-form-module.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchModuleComponent,
    PlanetsModuleComponent,
    SpeciesModuleComponent,
    StarshipsModuleComponent,
    VehiclesModuleComponent,
    FilmFormModuleComponent,
    PlanetFormModuleComponent,
    SpeciesFormModuleComponent,
    StarshipsFormModuleComponent,
    VehiclesFormModuleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [ComponentRegistrationService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetFormModuleComponent } from './planet-form-module.component';

describe('PlanetFormModuleComponent', () => {
  let component: PlanetFormModuleComponent;
  let fixture: ComponentFixture<PlanetFormModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanetFormModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetFormModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

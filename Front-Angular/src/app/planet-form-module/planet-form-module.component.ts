import { Router } from '@angular/router';
import { ComponentRegistrationService } from './../component-registration.service';
import { Planet } from './../models/planet';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-planet-form-module',
  templateUrl: './planet-form-module.component.html',
  styleUrls: ['./planet-form-module.component.css']
})
export class PlanetFormModuleComponent implements OnInit {
  public planet: Planet;
  constructor(private _service: ComponentRegistrationService, private _router:Router) { }

  ngOnInit(): void {
    this.planet = this._service.getter();
  }

  processForm(){
    this.splitArrays(this.planet);
    if(this.planet.id == undefined)
      this._service.createComponent(this.planet, "planets");
    else
      this._service.updateComponent(this.planet, "planets");
    this._router.navigate(['planets']);
  }

  splitArrays(planet: Planet){
    if(this.planet.films != undefined)
        this.planet.films = this.planet.films.toString().split(",");

      if(this.planet.residents != undefined)
        this.planet.residents = this.planet.residents.toString().split(',');
  }

  router(component: string){
    this._router.navigate([component]);
  }

}

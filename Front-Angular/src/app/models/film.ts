import { NumberSymbol } from "@angular/common";

export class Film{
  constructor(
      public id: any,
      public title: string,
      public episode_id: number,
      public opening_crawl: string,
      public director: string,
      public producer: string,
      public release_date: Date,
      public species: Array<string>,
      public starships: Array<string>,
      public vehicles: Array<String>,
      public characters: Array<string>,
      public planets: Array<string>,
      public url: string,
      public created: Date,
      public edited: Date
  ){}
}

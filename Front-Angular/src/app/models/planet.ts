export class Planet{
  constructor(
      public id: any,
      public name: string,
      public diameter: number,
      public rotation_period: string,
      public gravity: string,
      public population: string,
      public climate: string,
      public terrain: string,
      public surface_water: string,
      public residents: Array<String>,
      public films: Array<String>,
      public url: string,
      public created: Date,
      public edited: Date
  ){}
}

export class Starship{
  constructor(
      public id: any,
      public name: string,
      public starship_class: string,
      public manufacturer: string,
      public cost_in_credits: string,
      public length: string,
      public crew: string,
      public passengers: string,
      public max_atmosphering_speed: string,
      public hyperdrive_rating: string,
      public mglt: string,
      public cargo_capacity: string,
      public consumables: string,
      public films: Array<String>,
      public pilots: Array<String>,
      public url: string,
      public created: Date,
      public edited: Date
  ){}
}

export class Vehicle{
  constructor(
      public id: any,
      public model: string,
      public vehicle_class: string,
      public manufacturer: string,
      public length: string,
      public cost_in_credits: string,
      public crew: string,
      public passengers: string,
      public max_atmosphering_speed: string,
      public cargo_capacity: string,
      public consumables: string,
      public films: Array<String>,
      public pilots: Array<String>,
      public url: string,
      public created: Date,
      public edited: Date
  ){}
}

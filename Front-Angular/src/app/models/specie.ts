export class Specie{
  constructor(
      public id: any,
      public name: string,
      public classification: string,
      public average_height: string,
      public average_lifespan: string,
      public eye_colors: string,
      public hair_colors: string,
      public skin_colors: string,
      public language: string,
      public homeworld: string,
      public people: Array<String>,
      public films: Array<String>,
      public url: string,
      public created: Date,
      public edited: Date
  ){}
}

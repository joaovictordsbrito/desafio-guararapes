import { Router } from '@angular/router';
import { ComponentRegistrationService } from './../component-registration.service';
import { Starship } from './../models/starship';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-starships-form-module',
  templateUrl: './starships-form-module.component.html',
  styleUrls: ['./starships-form-module.component.css']
})
export class StarshipsFormModuleComponent implements OnInit {
  starship: Starship;
  constructor(private _service: ComponentRegistrationService, private _router:Router) { }

  ngOnInit(): void {
    this.starship = this._service.getter();
  }

  processForm(){
    this.splitArrays(this.starship);
    if(this.starship.id == undefined){
      this._service.createComponent(this.starship, "starships");
    }else{
      this._service.updateComponent(this.starship, "starships");
    }
    this._router.navigate(['starships']);
  }

  router(component: string){
    this._router.navigate([component]);
  }

  splitArrays(starship: Starship){
    if(this.starship.films != undefined){
      this.starship.films = this.starship.films.toString().split(',');
    }
    if(this.starship.pilots != undefined){
      this.starship.pilots = this.starship.pilots.toString().split(',');
    }
  }

}

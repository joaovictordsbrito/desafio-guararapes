import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StarshipsFormModuleComponent } from './starships-form-module.component';

describe('StarshipsFormModuleComponent', () => {
  let component: StarshipsFormModuleComponent;
  let fixture: ComponentFixture<StarshipsFormModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StarshipsFormModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StarshipsFormModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

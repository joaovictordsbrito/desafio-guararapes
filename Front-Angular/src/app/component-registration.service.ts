import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Film } from './models/film';
import { Planet } from './models/planet';
import { Specie } from './models/specie';
import { Starship } from './models/starship';
import { Vehicle } from './models/vehicle';

@Injectable({
  providedIn: 'root'
})
export class ComponentRegistrationService {

  private component: any;

  constructor(private http:HttpClient) { }

  public getAllFilms(){
    return this.http.get<Film[]>('http://localhost:8888/films/films/');
  }

  public getAllSpecies(){
    return this.http.get<Specie[]>('http://localhost:8888/species/species/');
  }

  public getAllVehicles(){
    return this.http.get<Vehicle[]>('http://localhost:8888/vehicles/vehicles/');
  }

  public getAllPlanets(){
    return this.http.get<Planet[]>('http://localhost:8888/planets/planets/');
  }

  public getAllStarships(){
    return this.http.get<Starship[]>('http://localhost:8888/starships/starships/');
  }

  public loadAllDataFromSwapi(){
    this.http.get('http://localhost:8888/integrador/films/').subscribe();
    this.http.get('http://localhost:8888/integrador/planets/').subscribe();
    this.http.get('http://localhost:8888/integrador/species/').subscribe();
    this.http.get('http://localhost:8888/integrador/starships/').subscribe();
    this.http.get('http://localhost:8888/integrador/vehicles/').subscribe();
    return "ok";
  }

  public deleteById(id: number, componentCommand: string){
    this.http.delete('http://localhost:8888/' + componentCommand + '/' + componentCommand + '/' + id).subscribe();
  }

  public updateComponent(component: any, componentCommand: string){
    const headers = {'Content-Type' : 'application/json'};
    this.http.put('http://localhost:8888/' + componentCommand + "/" + componentCommand, JSON.stringify(component), {'headers': headers}).subscribe();
  }

  public createComponent(component: any, componentCommand: string){
    const headers = {'Content-Type' : 'application/json'};
    this.http.post('http://localhost:8888/' + componentCommand + "/" + componentCommand, JSON.stringify(component), {'headers': headers}).subscribe();
  }

  getter(){
    return this.component;
  }

  setter(component: any){
    this.component = component;
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetsModuleComponent } from './planets-module.component';

describe('PlanetsModuleComponent', () => {
  let component: PlanetsModuleComponent;
  let fixture: ComponentFixture<PlanetsModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanetsModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetsModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

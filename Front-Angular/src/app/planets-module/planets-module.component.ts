import { Router } from '@angular/router';
import { Planet } from './../models/planet';
import { ComponentRegistrationService } from './../component-registration.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-planets-module',
  templateUrl: './planets-module.component.html',
  styleUrls: ['./planets-module.component.css']
})
export class PlanetsModuleComponent implements OnInit {

  planet: Planet[];

  constructor(private service:ComponentRegistrationService, private _router:Router) { }

  ngOnInit(): void {
    let response = this.service.getAllPlanets();
    response.subscribe((data)=>this.planet=data);
  }

  public getAllPlanets(){
    let response = this.service.getAllPlanets();
    response.subscribe((data)=>this.planet=data);
  }

  public integrator(){
    this.service.loadAllDataFromSwapi();
  }

  public createNewPlanet(){
    this.service.setter(new Planet(undefined, "", 0, "", "", "", "", "", "", new Array<string>(), new Array<string>(), "", new Date, new Date))
    this._router.navigate(['planets-form']);
  }

  public updatePlanet(planet: Planet){
    this.service.setter(planet);
    this._router.navigate(['planets-form']);
  }

  public delete(planet: Planet){
    this.service.deleteById(planet.id, "planets");
    this.ngOnInit();
    window.location.reload();
  }

  public router(component: string){
    this._router.navigate([component]);
  }

}

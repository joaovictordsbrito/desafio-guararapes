import { Router } from '@angular/router';
import { ComponentRegistrationService } from './../component-registration.service';
import { Vehicle } from './../models/vehicle';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vehicles-module',
  templateUrl: './vehicles-module.component.html',
  styleUrls: ['./vehicles-module.component.css']
})
export class VehiclesModuleComponent implements OnInit {

  vehicle: Vehicle[];
  constructor(private service:ComponentRegistrationService, private _router:Router) { }

  ngOnInit(): void {
    let response = this.service.getAllVehicles();
    response.subscribe((data)=>this.vehicle=data);
  }

  public integrator(){
    this.service.loadAllDataFromSwapi();
  }

  public delete(vehicle: Vehicle){
    this.service.deleteById(vehicle.id, "vehicles");
    this.ngOnInit();
    window.location.reload();
  }

  public createNewVehicle(){
    this.service.setter(new Vehicle(undefined, "", "", "", "", "", "", "", "", "", "", Array<string>(), Array<string>(), "", new Date, new Date));
    this._router.navigate(['/vehicles-form']);
  }

  public updateVehicle(vehicle: Vehicle){
    this.service.setter(vehicle);
    this._router.navigate(['/vehicles-form']);
  }

  public router(component: string){
    this._router.navigate([component]);
  }

}

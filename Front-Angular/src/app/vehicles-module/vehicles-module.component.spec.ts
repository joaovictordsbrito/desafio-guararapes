import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesModuleComponent } from './vehicles-module.component';

describe('VehiclesModuleComponent', () => {
  let component: VehiclesModuleComponent;
  let fixture: ComponentFixture<VehiclesModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehiclesModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

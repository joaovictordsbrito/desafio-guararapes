import { StarshipsFormModuleComponent } from './starships-form-module/starships-form-module.component';
import { VehiclesFormModuleComponent } from './vehicles-form-module/vehicles-form-module.component';
import { SpeciesFormModuleComponent } from './species-form-module/species-form-module.component';
import { PlanetFormModuleComponent } from './planet-form-module/planet-form-module.component';
import { FilmFormModuleComponent } from './film-form-module/film-form-module.component';
import { StarshipsModuleComponent } from './starships-module/starships-module.component';
import { VehiclesModuleComponent } from './vehicles-module/vehicles-module.component';
import { SpeciesModuleComponent } from './species-module/species-module.component';
import { PlanetsModuleComponent } from './planets-module/planets-module.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchModuleComponent } from './search-module/search-module.component';

const routes: Routes = [
  {path: "", redirectTo: "search", pathMatch:"full"},
  {path: "search", component:SearchModuleComponent},
  {path: "planets", component:PlanetsModuleComponent},
  {path: "species", component:SpeciesModuleComponent},
  {path: "vehicles", component:VehiclesModuleComponent},
  {path: "starships", component:StarshipsModuleComponent},
  {path: "film-form", component:FilmFormModuleComponent},
  {path: "planets-form", component:PlanetFormModuleComponent},
  {path: "species-form", component:SpeciesFormModuleComponent},
  {path: "vehicles-form", component:VehiclesFormModuleComponent},
  {path: "starships-form", component:StarshipsFormModuleComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

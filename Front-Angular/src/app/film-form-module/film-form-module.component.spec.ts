import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmFormModuleComponent } from './film-form-module.component';

describe('FilmFormModuleComponent', () => {
  let component: FilmFormModuleComponent;
  let fixture: ComponentFixture<FilmFormModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilmFormModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmFormModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Router } from '@angular/router';
import { ComponentRegistrationService } from './../component-registration.service';
import { Film } from './../models/film';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-film-form-module',
  templateUrl: './film-form-module.component.html',
  styleUrls: ['./film-form-module.component.css']
})
export class FilmFormModuleComponent implements OnInit {
  public film: Film;
  constructor(private _service: ComponentRegistrationService, private _router: Router) { }

  ngOnInit(): void {
    this.film = this._service.getter();
  }

  processForm(){
    this.splitArrays(this.film);
    if(this.film.id == undefined){
      this._service.createComponent(this.film, "films");
    }else{
      this._service.updateComponent(this.film, "films");
    }

    this._router.navigate(['search']);
  }

  router(component: string){
    this._router.navigate([component]);
  }

  splitArrays(film: Film){
    if(this.film.species != undefined)
      this.film.species = this.film.species.toString().split(",");

    if(this.film.starships != undefined)
      this.film.starships = this.film.starships.toString().split(",");

    if(this.film.vehicles != undefined)
      this.film.vehicles = this.film.vehicles.toString().split(",");

    if(this.film.characters != undefined )
      this.film.characters = this.film.characters.toString().split(",");

    if(this.film.planets != undefined)
      this.film.planets = this.film.species.toString().split(",");

  }

}

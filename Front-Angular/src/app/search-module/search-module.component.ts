import { Film } from './../models/film';
import { Component, OnInit } from '@angular/core';
import { ComponentRegistrationService } from '../component-registration.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-search-module',
  templateUrl: './search-module.component.html',
  styleUrls: ['./search-module.component.css']
})
export class SearchModuleComponent implements OnInit {

  film: Film[];

  constructor(private service:ComponentRegistrationService, private _router:Router) { }

  ngOnInit(): void {
    let response = this.service.getAllFilms();
    response.subscribe((data)=>this.film=data);
  }

  public getAllFilms(){
    let response = this.service.getAllFilms();
    response.subscribe((data)=>this.film=data);
  }

  public integrator(){
    this.service.loadAllDataFromSwapi();
  }

  public delete(film: Film){
    this.service.deleteById(film.id, "films");
    this.ngOnInit();
    window.location.reload();
  }

  public createNewFilm(){
    this.service.setter(new Film(undefined, "", 0, "", "", "", new Date, new Array<string>(),new Array<string>(),
    new Array<string>(), new Array<string>(), new Array<string>(), "", new Date, new Date ))
    this._router.navigate(['/film-form']);
  }

  public updateFilm(film: Film){
    this.service.setter(film);
    this._router.navigate(['/film-form']);
  }

  public router(component: string){
    this._router.navigate([component]);
  }
}

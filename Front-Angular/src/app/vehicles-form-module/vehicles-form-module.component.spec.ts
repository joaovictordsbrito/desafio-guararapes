import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesFormModuleComponent } from './vehicles-form-module.component';

describe('VehiclesFormModuleComponent', () => {
  let component: VehiclesFormModuleComponent;
  let fixture: ComponentFixture<VehiclesFormModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehiclesFormModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesFormModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Router } from '@angular/router';
import { ComponentRegistrationService } from './../component-registration.service';
import { Vehicle } from './../models/vehicle';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vehicles-form-module',
  templateUrl: './vehicles-form-module.component.html',
  styleUrls: ['./vehicles-form-module.component.css']
})
export class VehiclesFormModuleComponent implements OnInit {
  vehicle: Vehicle;
  constructor(private _service:ComponentRegistrationService, private _router:Router) { }

  ngOnInit(): void {
    this.vehicle = this._service.getter();
  }

  processForm(){
    this.splitArrays(this.vehicle);

    if(this.vehicle.id == undefined){
      this._service.createComponent(this.vehicle, "vehicles");
    }else{
      this._service.updateComponent(this.vehicle, "vehicles");
    }

    this._router.navigate(['vehicles']);
  }

  router(component: string){
    this._router.navigate([component]);
  }

  splitArrays(vehicle: Vehicle){
    if(this.vehicle.films != undefined){
      this.vehicle.films = this.vehicle.films.toString().split(',');
    }
    if(this.vehicle.pilots != undefined){
      this.vehicle.pilots = this.vehicle.pilots.toString().split(',');
    }
  }

}

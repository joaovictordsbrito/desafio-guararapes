import { Router } from '@angular/router';
import { ComponentRegistrationService } from './../component-registration.service';
import { Starship } from './../models/starship';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-starships-module',
  templateUrl: './starships-module.component.html',
  styleUrls: ['./starships-module.component.css']
})
export class StarshipsModuleComponent implements OnInit {

  starship: Starship[];

  constructor(private service:ComponentRegistrationService, private _router:Router) { }

  ngOnInit(): void {
    let response = this.service.getAllStarships();
    response.subscribe((data)=>this.starship=data);
  }

  public integrator(){
    this.service.loadAllDataFromSwapi();
  }

  public delete(starship: Starship){
    this.service.deleteById(starship.id, "starships");
    this.ngOnInit();
    window.location.reload();
  }

  public createNewStarship(){
    this.service.setter(new Starship(undefined, "", "", "", "", "", "", "", "", "", "", "", "", Array<string>(), Array<string>(), "", new Date, new Date));
    this._router.navigate(['/starships-form']);
  }

  public updateStarship(starship: Starship){
    this.service.setter(starship);
    this._router.navigate(['/starship-form']);
  }

  public router(component: string){
    this._router.navigate([component]);
  }

}

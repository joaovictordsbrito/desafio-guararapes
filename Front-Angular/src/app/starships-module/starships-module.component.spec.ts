import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StarshipsModuleComponent } from './starships-module.component';

describe('StarshipsModuleComponent', () => {
  let component: StarshipsModuleComponent;
  let fixture: ComponentFixture<StarshipsModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StarshipsModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StarshipsModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeciesModuleComponent } from './species-module.component';

describe('SpeciesModuleComponent', () => {
  let component: SpeciesModuleComponent;
  let fixture: ComponentFixture<SpeciesModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpeciesModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeciesModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Router } from '@angular/router';
import { ComponentRegistrationService } from './../component-registration.service';
import { Specie } from './../models/specie';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-species-module',
  templateUrl: './species-module.component.html',
  styleUrls: ['./species-module.component.css']
})
export class SpeciesModuleComponent implements OnInit {

  specie: Specie[];
  constructor(private service:ComponentRegistrationService, private _router:Router) { }

  ngOnInit(): void {
    let response = this.service.getAllSpecies();
    response.subscribe((data)=> this.specie=data);
  }

  getAllSpecies(){
    let response = this.service.getAllSpecies();
    response.subscribe((data)=> this.specie=data);
  }

  public integrator(){
    this.service.loadAllDataFromSwapi();
  }

  public createNewSpecies(){
    this.service.setter(new Specie(undefined, "", "", "", "", "", "", "", "" ,"" , new Array<string>(), new Array<string>(), "", new Date, new Date));
    this._router.navigate(['/species-form']);
  }

  public updateSpecies(specie: Specie){
    this.service.setter(specie);
    this._router.navigate(['/species-form']);
  }

  public delete(specie: Specie){
    this.service.deleteById(specie.id, "species");
    this.ngOnInit();
    window.location.reload();
  }

  public router(component: string){
    this._router.navigate([component]);
  }

}

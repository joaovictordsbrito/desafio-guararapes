package com.example.msstarship.repository;

import com.example.msstarship.entity.Starships;
import org.springframework.data.repository.CrudRepository;

public interface StarshipRepository extends CrudRepository<Starships, Integer> {
}

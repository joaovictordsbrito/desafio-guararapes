package com.example.msstarship.controller;

import com.example.msstarship.entity.Starships;
import com.example.msstarship.repository.StarshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/starships")
public class StarshipController {

    @Autowired
    private StarshipRepository starshipRepository;

    @RequestMapping(method = RequestMethod.POST)
    Starships create (@RequestBody Starships starship){
        return starshipRepository.save(starship);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveAll")
    Iterable<Starships> saveAll (@RequestBody Iterable<Starships> starships){
        return starshipRepository.saveAll(starships);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    Optional<Starships> findById(@PathVariable Integer id){
        return starshipRepository.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    Iterable<Starships> findAll(){
        return starshipRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT)
    Starships update (@RequestBody Starships starships){
        return starshipRepository.save(starships);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    void deleteById(@PathVariable Integer id){
        starshipRepository.deleteById(id);
    }


}
